# Subtitles Contact Sheet

Script written in **BASH** that generates previews of the subtitles of a video. The script reads the subtitle file and selects frames from the video where subtitles appear to generate an image with all these frames.

The purpose of the script is to create an image that shows a preview of the video and subtitles with basic subtitle statistics. It can also generate previews of the video only displaying the basic data of the streams contained in the video. It is also possible to apply filters on the VR videos to make the screenshots easy to watch

There are an improved version in C++ [Subtitles Contact Sheet QT](https://gitlab.com/vongooB9/subtitles_contact_sheet_qt)

## Tested systems

I tested the script correctly in:

- Debian statble
- Debian testing
- Void Linux
- Windows 11 WSL Ubuntu

## Dependencies

It uses **ffmpeg** and **ffprobe** and several common utilities such as **bc**, **sed**, **cut**, **tr**...

In Debian needs the packages: ffmpeg, bc, sed and fontconfig

## Suported formarts

It supports subtitles in **srt**, **ass** and a basic **vtt** format. Supports video formats recognized by ffmpeg.

As output it can generate **jpg**, **png**, **webp**. But it could use any format that supports ffmpeg

## Usage

The easiest way to use it is when the video and subtitle files have the same name and differ only in the language code and extension.

- VideoName.mp4
- VideoName.en.srt

In this case you only have to run the script passing it as the only parameter the subtitles file

    subtitles_contact_sheet VideoName.en.srt

The script will generate the file *VideoName.en.srt.contact_sheet.jpg*

It can also work with just the video as follows

    subtitles_contact_sheet VideoName.mp4

In this case it will generate *VideoName.mp4.contact_sheet.jpg*


### Basic options

Summary with the most important options of the script

#### Input/Output files

- **-i or -sub <file>:** To indicate the subtitle file
- **-v or -video <file>:** To indicate the video file
- **-o or -out <file>:** To indicate the output file
- **-out_dir <dir>:** To indicate the output directory, only works without -out

#### Modes of operation

There are two different ways to select the subtitle lines to be captured. Using the **time** or using the subtitle **lines**

##### Time Mode

In this mode, the total time of the video is divided into as many segments as you want to capture, and for each one the first subtitle that completes the requirements is searched, if there is none, the first frame of the segment is captured.

This is the default mode, it can be forced with the **-time** option.

Parameters with specific behavior for this mode:
- **-start <arg>:** Initial part that is excluded from the screenshots, in seconds, default 30
- **-end <arg>:** In seconds, when it is positive it is the end of the screenshots, if it is negative it is subtracted from the total duration and the result is the end of the screenshots default -30

##### Line Mode

In this mode, the subtitle lines that meet the criteria are selected first, then a line is searched every few lines to complete the captures. In this mode all output images have subtitles, but large portions of the video are not represented because they do not have or have very few subtitles.

It is used by indicating the option **-line**.

Parameters with specific behavior for this mode:
- **-start <arg>:** Is the line on which the captures start, defailt 1
- **-end <arg>:** Is the line where the screenshots end, if negative, it is subtracted from the last line, default last line

#### Output options

- **-l or -layout <arg>:** Number of captured frames in the format COLUMSxROWS, default 3x6
- **-s or -size <arg>:** Size in pixels of the width for captured frames, default 600
- **-hide_time:** Don't display the time of the frames
- **-png :** Set output format png
- **-jpg or -jpeg :** Set output format jpg, default
- **-webp :** Set output format webp

#### Header options

These options configure the text block with statistics and subtitle details.

- **-text_block <arg>:** Can be top, bottom or hidden, default top
- **-t or -title <arg>:** Set the title, default use the subtitle file name
- **-c or -comment <arg>:** Set the commentary in the text header, it appears on the last line
- **-ct or -comment_title <arg>:** Set the commentary title in the text header
- **-logo <file>:** Put this logo in the header aligned to the right and rescaled to the header height

#### Style options

##### Subtitles simple options

- **-green :** Green subtitles text. Same as -sub_color 00FF00
- **-yellow :** Yellow subtitles text. Same as -sub_color 00EAFF
- **-small :** Small subtitle text. Same as -sub_size 12
- **-big :** Big subtitle text. Same as -sub_size 24
- **-verybig :** Very big subtitle text. Same as -sub_size 32
- **-box :** Subtitle text inside a black box. Same as -sub_border 4
- **-tbox :** Subtitle text inside a black transparent box -sub_border 4 -sub_bc 50000000

##### Header and grid

- **-font <arg>:** Set the font for text header.
- **-fontsize <arg>:** Set the font height for text header.
- **-color <arg>:** Set the color of the font for text header in BBGGRR format. Default White
- **-bg <arg>:** Set the background color for text header and grid in RRGGBB format. Default Black
- **-grid <arg>:** Set border for the grid in pixels



#### VR options

These options use the ffmpeg **v360** filter. They are a simplification of what you can do, the full documentation is at the following link

https://ffmpeg.org/ffmpeg-filters.html#v360

By default it is configured to process 180° stereo videos


- **-vr:** Enables the VR correction filter
- **-vr_in <arg>:** Input format,
- **-vr_out <arg>:** Output format, Default flat
- **-vr_in_stereo <arg>:** Input stereo mode, can be sbs, tb, 2d, Default sbs
- **-vr_out_stereo <arg>:** Output stereo mode, Same options than input Default 2d
- **-vr_id_fov <arg>:** Set the input field of view in degrees Default: 180
- **-vr_d_fov <arg>:** Set the output field of view in degrees Default 130

*Rotation:*
- **-vr_yaw <arg>:**
- **-vr_pitch <arg>:**
- **-vr_roll <arg>:**

*Resolution:*
- **-vr_w <arg>:** Set the output resolution width Default 800
- **-vr_h <arg>:** Set the output resolution height Default 800

This resolution is not the size of the screenshots, just an intermediate step, it can be the same but I think it works better if it is larger than the size of the screenshots

- **-vr_interp <arg>:** Interpolation method Default lanczos
- **-vr_h_flip:** Flip the output video horizontally
- **-only_keyframes:** Try to capture only keyframes, sometimes this is faster but not work with subtitles
